import 'package:flutter/material.dart';

import '../screens/cart_creen.dart';
import '../screens/orders_screen.dart';
import '../screens/user_products_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(IconData icon, String title, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'Lato',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        child: Column(
          children: <Widget>[
            /* Container(
              height: 120,
              width: double.infinity,
              padding: EdgeInsets.all(20),
              alignment: Alignment.centerLeft,
              color: Theme.of(context).primaryColor,
              child: Text(
                'MyShop',
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.white,
                ),
              ),
            ), */
            AppBar(
              title: Text('MyShop'),
              automaticallyImplyLeading: false,
            ),
            SizedBox(
              height: 20,
            ),
            buildListTile(
              Icons.shop,
              'Home',
              () {
                Navigator.of(context).pushReplacementNamed('/');
              },
            ),
            Divider(),
            buildListTile(
              Icons.shopping_cart,
              'Cart',
              () {
                Navigator.of(context)
                    .pushReplacementNamed(CartScreen.routeName);
              },
            ),
            Divider(),
            buildListTile(
              Icons.payment,
              'Orders',
              () {
                Navigator.of(context)
                    .pushReplacementNamed(OrdersScreen.routeName);
              },
            ),
            Divider(),
            buildListTile(
              Icons.edit,
              'Manage products',
              () {
                Navigator.of(context)
                    .pushReplacementNamed(UserProductsScreen.routeName);
              },
            ),
            Divider(),
          ],
        ),
      ),
    );
  }
}
