import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../screens/edit_product_screen.dart';
import '../providers/products_provider.dart';

class UserProductItem extends StatelessWidget {
  final String id;
  final String title;
  final String imageUrl;

  UserProductItem(
    this.id,
    this.title,
    this.imageUrl,
  );

  @override
  Widget build(BuildContext context) {
    final scaffoldContext = Scaffold.of(context);
    final navigatorContext = Navigator.of(context);

    return ListTile(
      title: Text(title),
      leading: CircleAvatar(
        backgroundImage: NetworkImage(imageUrl),
      ),
      trailing: Container(
        width: 100,
        child: Row(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.edit),
              onPressed: () {
                Navigator.of(context)
                    .pushNamed(EditProductScreen.routeName, arguments: id);
              },
              color: Theme.of(context).primaryColor,
            ),
            IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (ctx) => AlertDialog(
                    title: Text('Confirm deletion'),
                    content: Text('Do you really want to delete product?'),
                    actions: <Widget>[
                      FlatButton(
                        child: Text("No"),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      FlatButton(
                        child: Text("Yes"),
                        onPressed: () async {
                          try {
                            await Provider.of<ProductsProvider>(context,
                                    listen: false)
                                .deleteProduct(id);
                          } catch (error) {
                            scaffoldContext.hideCurrentSnackBar();
                            scaffoldContext.showSnackBar(
                              SnackBar(
                                content: Text(
                                  'Deleting failed.',
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            );
                          }
                          navigatorContext.pop();
                        },
                      )
                    ],
                  ),
                );
              },
              color: Theme.of(context).errorColor,
            ),
          ],
        ),
      ),
    );
  }
}
