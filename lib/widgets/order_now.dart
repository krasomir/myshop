import 'package:flutter/material.dart';

import '../providers/cart_provider.dart';
import '../providers/orders_provider.dart';

class OrderNow extends StatefulWidget {
  const OrderNow({
    Key key,
    @required this.cart,
    @required this.orderContext,
  }) : super(key: key);

  final CartProvider cart;
  final OrdersProvider orderContext;

  @override
  _OrderNowState createState() => _OrderNowState();
}

class _OrderNowState extends State<OrderNow> {
  var _isLoading = false;

  @override
  Widget build(BuildContext context) {
    final scaffoldContext = Scaffold.of(context);

    return FlatButton(
      child: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Text('ORDER NOW'),
      onPressed: (widget.cart.itemCount > 0 || _isLoading)
          ? () async {
              setState(() {
                _isLoading = true;
              });
              try {
                await widget.orderContext.addOrder(
                  widget.cart.items.values.toList(),
                  widget.cart.totalAmount,
                );
                widget.cart.clear();
                setState(() {
                  _isLoading = false;
                });
              } catch (error) {
                setState(() {
                  _isLoading = false;
                });
                scaffoldContext.hideCurrentSnackBar();
                scaffoldContext.showSnackBar(
                  SnackBar(
                    content: Text(
                      error.toString(),
                      textAlign: TextAlign.center,
                    ),
                  ),
                );
              }
            }
          : null,
      textColor: Theme.of(context).primaryColor,
    );
  }
}
