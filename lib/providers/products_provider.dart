import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../models/http_exception.dart';
import './product_provider.dart';

class ProductsProvider with ChangeNotifier {
  List<ProductProvider> _items = [
    /* ProductProvider(
      id: 'p1',
      title: 'Red Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
          'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
    ),
    ProductProvider(
      id: 'p2',
      title: 'Trousers',
      description: 'A nice pair of trousers.',
      price: 59.99,
      imageUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Trousers%2C_dress_%28AM_1960.022-8%29.jpg/512px-Trousers%2C_dress_%28AM_1960.022-8%29.jpg',
    ),
    ProductProvider(
      id: 'p3',
      title: 'Yellow Scarf',
      description: 'Warm and cozy - exactly what you need for the winter.',
      price: 19.99,
      imageUrl:
          'https://live.staticflickr.com/4043/4438260868_cc79b3369d_z.jpg',
    ),
    ProductProvider(
      id: 'p4',
      title: 'A Pan',
      description: 'Prepare any meal you want.',
      price: 49.99,
      imageUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Cast-Iron-Pan.jpg/1024px-Cast-Iron-Pan.jpg',
    ),
   */
  ];

  List<ProductProvider> get items {
    return [..._items];
  }

  List<ProductProvider> get favoriteItems {
    return _items.where((item) => item.isFavorite).toList();
  }

  ProductProvider findById(String id) {
    return _items.firstWhere((item) => item.id == id);
  }

  Future<void> fetchAndSetProducts() async {
    const url =
        'https://flutter-update-7f150-default-rtdb.europe-west1.firebasedatabase.app/product.json';
    try {
      final response = await http.get(url);
      final extractedData = jsonDecode(response.body) as Map<String, dynamic>;
      final List<ProductProvider> loadedProduct = [];

      if (extractedData == null) {
        return;
      }

      extractedData.forEach((productId, productData) {
        loadedProduct.add(ProductProvider(
          id: productId,
          title: productData['title'],
          description: productData['description'],
          price: productData['price'],
          imageUrl: productData['imageUrl'],
          isFavorite: productData['isFavorite'],
        ));
      });

      _items = loadedProduct;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<void> addProduct(ProductProvider product) async {
    const url =
        'https://flutter-update-7f150-default-rtdb.europe-west1.firebasedatabase.app/product.json';
    try {
      final response = await http.post(
        url,
        body: json.encode({
          'title': product.title,
          'description': product.description,
          'price': product.price,
          'imageUrl': product.imageUrl,
          'isFavorite': product.isFavorite,
        }),
      );
      final newProduct = ProductProvider(
        title: product.title,
        price: product.price,
        description: product.description,
        imageUrl: product.imageUrl,
        id: json.decode(response.body)['name'],
      );
      _items.add(newProduct); // ads new product at the and of list
      //_items.insert(0, newProduct); // ads new product at the start of a list
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<void> updateProduct(
      String id, ProductProvider newProductValues) async {
    final prodIndex = _items.indexWhere((product) => product.id == id);
    if (prodIndex >= 0) {
      final url =
          'https://flutter-update-7f150-default-rtdb.europe-west1.firebasedatabase.app/product/$id.json';
      try {
        await http.patch(
          url,
          body: json.encode({
            'title': newProductValues.title,
            'description': newProductValues.description,
            'imageUrl': newProductValues.imageUrl,
            'price': newProductValues.price,
          }),
        );
        _items[prodIndex] = newProductValues;
        notifyListeners();
      } catch (error) {
        throw error;
      }
    }
  }

  // for show, still using 'optimistic update' pattern but with async await
  Future<void> deleteProduct(String id) async {
    final url =
        'https://flutter-update-7f150-default-rtdb.europe-west1.firebasedatabase.app/product/$id.json';
    // get index of product which will be deleted
    final existingProductIndex =
        _items.indexWhere((product) => product.id == id);
    // get a reference od that product
    var existingProduct = _items[existingProductIndex];
    // remove product with that index from lis, bat not from memory
    _items.removeAt(existingProductIndex);
    notifyListeners();

    final response = await http.delete(url);

    if (response.statusCode >= 400) {
      // if deleting on server was not success,
      // then insert existing product on an existing index in list
      _items.insert(existingProductIndex, existingProduct);
      notifyListeners();

      throw HttpException('Could not delete product.');
    }
    // if deleting on server is success, then set existing product to null
    // this will remove reference to product and dart wil delete product from memory
    existingProduct = null;
  }
}
