import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import '../models/http_exception.dart';

class ProductProvider with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavorite;

  ProductProvider({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.price,
    @required this.imageUrl,
    this.isFavorite = false,
  });

  void _setFavValue(bool newValue) {
    isFavorite = newValue;
    notifyListeners();
  }

  Future<void> toggleFavoriteStatus(ProductProvider product) async {
    final url =
        'https://flutter-update-7f150-default-rtdb.europe-west1.firebasedatabase.app/product/${product.id}.json';
    final oldFavStatus = isFavorite;
    _setFavValue(!isFavorite);

    try {
      final response = await http.patch(
        url,
        body: json.encode({
          'isFavorite': isFavorite,
        }),
      );

      if (response.statusCode >= 400) {
        _setFavValue(oldFavStatus);

        throw HttpException(
            'Could not change favorite status. Error: status code.');
      }
    } catch (error) {
      _setFavValue(oldFavStatus);
      throw HttpException('Could not change favorite status.');
    }
  }
}
